import java.util.*;
import java.util.stream.Collectors;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        int[] nums = new int[]{1, 1, 1, 3, 3, 4, 2, 4, 2};
        int[] nums2 = new int[]{1, 2, 3, 4, 5};
        System.out.println(containsDuplicate2(nums));
    }

    private boolean containsDuplicate(int[] nums) {
        /*if(nums==null || nums.length==0)
            return false;*/
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == nums[i + 1])
                return true;
        }
        return false;
    }


/*
    private boolean containsDuplicate(int[] nums){
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            //better option
            if(!set.add(num))
                return true;
            *//*if(set.contains(num))
                return true;
            set.add(num);*//*
        }
        return false;
    }*/


    //worst effectiveness
    /*private boolean containsDuplicate(int[] nums) {
        if (nums==null || nums.length==0)
        return false;

        for (int i = 0; i < nums.length; i++) {
            for(int j=i+1; j<nums.length;j++){
                if (nums[i]==nums[j])
                    return true;
            }
        }

        return false;
    }*/

    //the best solution, similar to the worst
    private boolean containsDuplicate2(int[] nums) {
        if (nums == null || nums.length == 0)
            return false;

        for (int i = 1; i < nums.length; i++) {
            for (int j = i - 1; j >= 0; j--) {
                if (nums[i]>nums[j])break;
                if (nums[i] == nums[j])
                    return true;
            }
        }

        return false;
    }
}
